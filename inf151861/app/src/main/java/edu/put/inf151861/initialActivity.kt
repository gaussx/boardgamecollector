package edu.put.inf151861

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.w3c.dom.Text
import java.io.File
import java.io.FileWriter
import java.net.MalformedURLException
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory


class initialActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial)
    }

    fun getUserXML(username: String): String{
        val urlString = "https://www.boardgamegeek.com/xmlapi2/collection?username=$username"
        val xmlDirectory = File("$filesDir/XML")
        if(!xmlDirectory.exists()) xmlDirectory.mkdir()
        val fileName = "$xmlDirectory/$username.xml"

        CoroutineScope(Dispatchers.IO).launch{
            try{
                val url = URL(urlString)
                val reader = url.openStream().bufferedReader()
                val downloadFile = File(fileName).also {it.createNewFile()}
                val writer = FileWriter(downloadFile).buffered()
                var line: String
                while(reader.readLine().also {line = it?.toString() ?: ""} != null)
                    writer.write(line)
                reader.close()
                writer.close()
                Log.i("Success", "Saved file $fileName")
            } catch (e: Exception) {
                withContext(Dispatchers.Main){
                    when(e){
                        is MalformedURLException -> print("Malformed URL.")
                        else -> print("Error.")
                    }
                }
                val incompleteFile = File(fileName)
                if(incompleteFile.exists()) incompleteFile.delete()
                Log.i("Failure", "Failed to obtain file.")
            }
        }

        return fileName
    }

    fun initializeDatabase(v: View){


        val username = findViewById<TextView>(R.id.enterUsernameText).text.toString()
        if(username.length == 0){
            Toast.makeText(this, "Please enter a username.", Toast.LENGTH_SHORT).show()
        }
        val filename = getUserXML(username)
//        val filename = "$filesDir/XML/loutre_on_fire.xml"
        val dbHandler = DBHandler(this, null, null, 1)
        val statusText = findViewById<TextView>(R.id.initialStatusText)
        statusText.text = "Fetching data"
        var userFile = File(filename)

        val timeout = 15000
        val increment = 300
        var current_time = 0
        while(!userFile.exists() && current_time < 15000){
            Thread.sleep(increment.toLong())
            current_time += increment
            userFile = File(filename)
        }

        statusText.text = "Building database"

        if(userFile.exists()){
            val userXML: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(userFile)
            userXML.documentElement.normalize()

            val items: NodeList = userXML.getElementsByTagName("item")
            var newGame: Game
            for(i in 0..items.length-1){
                var gameName = "placeholder"
                var gameType = "placeholder"
                var gameImage: String = "defaultimage.jpg"
                var gameYear: String = "1800"
                val gameDescription: String = "Placeholder Description"

                val itemNode: Node = items.item(i)
                if(itemNode.nodeType == Node.ELEMENT_NODE){
                    val elem = itemNode as Element
                    val children = elem.childNodes
                    val attributes = elem.attributes
                    gameType = attributes.getNamedItem("subtype").nodeValue.toString()
                    Log.i("type", gameType)
                    for(j in 0..children.length - 1){
                        val node = children.item(j)
                        if(node is Element){
                            when(node.nodeName){
                                "name" -> {
                                    Log.i("name", node.textContent)
                                    gameName = node.textContent
                                }
                                "yearpublished" -> {
                                    Log.i("year", node.textContent)
                                    gameYear = node.textContent
                                }
                                "image" -> {
                                    Log.i("image", node.textContent)
                                    gameImage = node.textContent
                                }
                            }
                        }
                    }
                    newGame = Game(gameName, gameType, gameImage, gameDescription, gameYear)
                    dbHandler.addGame(newGame)
                }
            }
        } else{
            Log.i("No such file", "File $filename not found.")
        }


        super.finish()
    }
}