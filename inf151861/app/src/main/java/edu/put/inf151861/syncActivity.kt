package edu.put.inf151861

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.File
import java.io.FileWriter
import java.net.MalformedURLException
import java.net.URL
import java.util.Date
import javax.xml.parsers.DocumentBuilderFactory

class syncActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sync)

        val button = findViewById<Button>(R.id.proceedButton)
        button.visibility = View.GONE

        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)

        val width = dm.widthPixels
        val height = dm.heightPixels
        window.setLayout((width*0.8).toInt(), (height*0.6).toInt())


        val userXMLFile = File("$filesDir/XML").listFiles()?.get(0)
        val lastModificationDate: Date?
        val diff: Int = 0
        if(userXMLFile != null){
            val lastModificationDate: Date = Date(userXMLFile.lastModified())
            val diff: Long = Date().time - lastModificationDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            if(hours < 24){
                val text = findViewById<TextView>(R.id.popupText)
                text.text = "Last synchronization was done less than 24 hours ago. Proceed anyway?"
                button.visibility = View.VISIBLE
            }
            else{
                proceed()
            }
        }
    }

    fun getUserXML(username: String): String{
        val urlString = "https://www.boardgamegeek.com/xmlapi2/collection?username=$username"
        val xmlDirectory = File("$filesDir/XML")
        if(!xmlDirectory.exists()) xmlDirectory.mkdir()
        val fileName = "$xmlDirectory/$username.xml"

        CoroutineScope(Dispatchers.IO).launch{
            try{
                val url = URL(urlString)
                val reader = url.openStream().bufferedReader()
                val downloadFile = File(fileName).also {it.createNewFile()}
                val writer = FileWriter(downloadFile).buffered()
                var line: String
                while(reader.readLine().also {line = it?.toString() ?: ""} != null)
                    writer.write(line)
                reader.close()
                writer.close()
                Log.i("Success", "Saved file $fileName")
            } catch (e: Exception) {
                withContext(Dispatchers.Main){
                    when(e){
                        is MalformedURLException -> print("Malformed URL.")
                        else -> print("Error.")
                    }
                }
                val incompleteFile = File(fileName)
                if(incompleteFile.exists()) incompleteFile.delete()
                Log.i("Failure", "Failed to obtain file.")
            }
        }

        return fileName
    }

    fun proceed(){
        val button = findViewById<Button>(R.id.proceedButton)
        button.visibility = View.GONE

        val text = findViewById<TextView>(R.id.popupText)
        text.text = "Verifying files..."

        val userXMLFile = File("$filesDir/XML").listFiles()?.get(0)
        var username: String? = null
        var filename = "placeholder"
        if(userXMLFile != null){
            username = userXMLFile.name.dropLast(4)
        }
        else{
            Toast.makeText(this, "Failed to load user file. Aborting.", Toast.LENGTH_SHORT).show()
        }

        if(username != null){
            filename = getUserXML(username)
        }

//        val filename = "$filesDir/XML/loutre_on_fire.xml"
        val dbHandler = DBHandler(this, null, null, 1)
        text.text = "Fetching data..."
        var userFile = File(filename)

        val timeout = 15000
        val increment = 300
        var current_time = 0
        while(!userFile.exists() && current_time < 15000){
            Thread.sleep(increment.toLong())
            current_time += increment
            userFile = File(filename)
        }

        text.text = "Building database..."

        if(userFile.exists()){
            dbHandler.deleteTable()
            val userXML: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(userFile)
            userXML.documentElement.normalize()

            val items: NodeList = userXML.getElementsByTagName("item")
            var newGame: Game
            for(i in 0..items.length-1){
                var gameName = "placeholder"
                var gameType = "placeholder"
                var gameImage: String = "defaultimage.jpg"
                var gameYear: String = "1800"
                val gameDescription: String = "Placeholder Description"

                val itemNode: Node = items.item(i)
                if(itemNode.nodeType == Node.ELEMENT_NODE){
                    val elem = itemNode as Element
                    val children = elem.childNodes
                    val attributes = elem.attributes
                    gameType = attributes.getNamedItem("subtype").nodeValue.toString()
                    Log.i("type", gameType)
                    for(j in 0..children.length - 1){
                        val node = children.item(j)
                        if(node is Element){
                            when(node.nodeName){
                                "name" -> {
                                    Log.i("name", node.textContent)
                                    gameName = node.textContent
                                }
                                "yearpublished" -> {
                                    Log.i("year", node.textContent)
                                    gameYear = node.textContent
                                }
                                "image" -> {
                                    Log.i("image", node.textContent)
                                    gameImage = node.textContent
                                }
                            }
                        }
                    }
                    newGame = Game(gameName, gameType, gameImage, gameDescription, gameYear)
                    dbHandler.addGame(newGame)
                }
            }
        } else{
            Log.i("No such file", "File $filename not found.")
        }
        Thread.sleep(300) //this is crucial for smooth operation
        super.finish()
    }

    fun proceed(v: View){
        proceed()
    }
}