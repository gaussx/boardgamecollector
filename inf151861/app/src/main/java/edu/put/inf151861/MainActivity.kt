package edu.put.inf151861

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context
import android.content.ContentValues
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.TextView
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

class Game{
    var id: Int = -1
    var name: String? = null
    var type: String? = null
    var image: String? = null
    var description: String? = null
    var year: String? = null

    constructor(id: Int, name: String, type: String, image: String, description: String, year: String){
        this.id = id
        this.name = name
        this.type = type
        this.image = image
        this.description = description
        this.year = year
    }

    constructor(name: String, type: String, image: String, description: String, year: String){
        this.name = name
        this.type = type
        this.image = image
        this.description = description
        this.year = year
    }
}

class DBHandler(context: Context, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int): SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION){
    companion object{
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "userDB.db"
        val TABLE_COLLECTION = "collection"
        val COLUMN_ID = "_id"
        val COLUMN_NAME = "name"
        val COLUMN_TYPE = "type"
        val COLUMN_IMAGE = "image"
        val COLUMN_DESCRIPTION = "description"
        val COLUMN_YEAR = "year"
    }

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_PRODUCTS_TABLE = ("CREATE TABLE " + TABLE_COLLECTION + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_NAME + " TEXT,"
                + COLUMN_TYPE + " TEXT,"
                + COLUMN_IMAGE + " TEXT,"
                + COLUMN_DESCRIPTION + " TEXT,"
                + COLUMN_YEAR + " TEXT)")
        db.execSQL(CREATE_PRODUCTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTION)
        onCreate(db)
    }

    fun addGame(game: Game){
        val values = ContentValues()
        values.put(COLUMN_NAME, game.name)
        values.put(COLUMN_TYPE, game.type)
        values.put(COLUMN_IMAGE, game.image)
        values.put(COLUMN_DESCRIPTION, game.description)
        values.put(COLUMN_YEAR, game.year)

        val db = this.writableDatabase
        db.insert(TABLE_COLLECTION, null, values)
        db.close()
    }

    fun findGame(gameName: String): Game?{
        val query = "SELECT * FROM $TABLE_COLLECTION WHERE $COLUMN_NAME LIKE \"$gameName\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        var game: Game? = null

        if(cursor.moveToFirst()){
            val id = Integer.parseInt(cursor.getString(0))
            val name = cursor.getString(1)
            val type = cursor.getString(2)
            val image = cursor.getString(3)
            val description = cursor.getString(4)
            val year = cursor.getString(5)

            game = Game(id, name, type, image, description, year)
            cursor.close()
        }

        db.close()
        return game
    }

    fun getNthProduct(type: String, index: Int): Game? {
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_COLLECTION WHERE $COLUMN_TYPE LIKE \"$type\"", null)
        cursor.moveToFirst()
        for(i in 0..index){
            cursor.moveToNext()
        }
        val id = Integer.parseInt(cursor.getString(0))
        val name = cursor.getString(1)
        val type = cursor.getString(2)
        val image = cursor.getString(3)
        val description = cursor.getString(4)
        val year = cursor.getString(5)

        val game = Game(id, name, type, image, description, year)
        cursor.close()
        return game
    }

    fun getRowCount(): Int{
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_COLLECTION", null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun getGameCount(): Int{
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_COLLECTION WHERE $COLUMN_TYPE LIKE \"boardgame\"", null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun getExpansionCount(): Int{
        val db = this.writableDatabase //note that this is a placeholder function for now
        val cursor = db.rawQuery("SELECT * FROM $TABLE_COLLECTION WHERE $COLUMN_TYPE LIKE \"boardgameexpansion\"", null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun deleteTable(){
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_COLLECTION WHERE 1 = 1")
        db.close()
    }


}



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dbHandler = DBHandler(this, null, null, 1)
        if(dbHandler.getRowCount() == 0){
            startActivity(Intent(this, initialActivity::class.java))
        }
        else{
            Log.i("Read success", "Successfully read ${dbHandler.getRowCount()} games.")
            findViewById<TextView>(R.id.gameAmountText).text = "Games collected: ${dbHandler.getGameCount()}"
            findViewById<TextView>(R.id.expansionAmountText).text = "Game expansions collected: ${dbHandler.getExpansionCount()}"
            val userXMLFile = File("$filesDir/XML").listFiles()?.get(0)
            var username: String? = null
            if(userXMLFile != null){
                username = userXMLFile.name.dropLast(4)
                val lastModificationDate: Date = Date(userXMLFile.lastModified())
                val prettyDate = lastModificationDate.toString().substring(0,3) + ", " + lastModificationDate.toString().substring(4, 10) + ' ' + lastModificationDate.toString().substring(30, 34) + ", " + lastModificationDate.toString().substring(11, 19)
                findViewById<TextView>(R.id.syncDateText).text = prettyDate
            }
            if(username != null)
                findViewById<TextView>(R.id.usernameText).text = "Username: $username"
        }
    }

    override fun onResume() {
        super.onResume()
        val dbHandler = DBHandler(this, null, null, 1)
        if(dbHandler.getRowCount() != 0){
            Log.i("Read success", "Successfully read ${dbHandler.getRowCount()} games.")
            findViewById<TextView>(R.id.gameAmountText).text = "Games collected: ${dbHandler.getGameCount()}"
            findViewById<TextView>(R.id.expansionAmountText).text = "Game expansions collected: ${dbHandler.getExpansionCount()}"
            val userXMLFile = File("$filesDir/XML").listFiles()?.get(0)
            var username: String? = null
            if(userXMLFile != null){
                username = userXMLFile.name.dropLast(4)
                val lastModificationDate: Date = Date(userXMLFile.lastModified())
                val prettyDate = lastModificationDate.toString().substring(0,3) + ", " + lastModificationDate.toString().substring(4, 10) + ' ' + lastModificationDate.toString().substring(30, 34) + ", " + lastModificationDate.toString().substring(11, 19)
                findViewById<TextView>(R.id.syncDateText).text = prettyDate
            }
            if(username != null)
                findViewById<TextView>(R.id.usernameText).text = "Username: $username"
        }
    }

    fun deleteData(v: View){
        val dbHandler = DBHandler(this, null, null, 1)
        dbHandler.deleteTable()
        val userXMLFolder = File("$filesDir/XML")
        for(child in userXMLFolder.listFiles())
            child.delete()
        val userDatabaseFolder = File("$filesDir/databases")
        for(child in userDatabaseFolder.listFiles())
            child.delete()
        super.finish()
    }

    fun syncData(v: View){
        startActivity(Intent(this, syncActivity::class.java))
    }

    fun showGameCollection(v: View){
        startActivity(Intent(this, gameCollectionActivity::class.java))
    }
}