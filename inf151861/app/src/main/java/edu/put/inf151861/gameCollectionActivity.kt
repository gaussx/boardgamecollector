package edu.put.inf151861

import android.os.Bundle
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class gameCollectionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_collection)

        val table = findViewById<TableLayout>(R.id.gameTable)
        table.removeAllViews()

        val dbHandler = DBHandler(this, null, null, 1)

        for (i in 0 until dbHandler.getGameCount()) {
            val currentGame = dbHandler.getNthProduct("boardgame", i)
            val number = i + 1
            val row = TableRow(this)

            val tv1 = TextView(this)
            tv1.text = number.toString()
            row.addView(tv1)

            val tv2 = TextView(this)
            if(currentGame != null)
                tv2.text = currentGame.name
            else
                tv2.text = "[FAILED TO LOAD]"
            row.addView(tv2)

            val tv3 = TextView(this)
            if(currentGame != null)
                tv3.text = currentGame.image
            else
                tv3.text = "[FAILED TO LOAD]"
            row.addView(tv3)

            table.addView(row)
        }
    }
}